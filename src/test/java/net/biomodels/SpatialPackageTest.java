package net.biomodels;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.sbml.jsbml.Compartment;
import org.sbml.jsbml.ListOf;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ext.spatial.SpatialModelPlugin;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Mihai Glonț <mglont@ebi.ac.uk>
 *         Date: 15/01/15
 */
@RunWith(JUnit4.class)
public class SpatialPackageTest {
    private final App app = new App();
    private Model m;

    @Before
    public void setUp() {
        String fileName = "fish_skin_pnas_vcell.xml";
        m = app.unmarshall(fileName);
    }

    @After
    public void tearDown() {
        m = null;
    }

    @Test
    public void testSpatial() {
        assertNotNull(m);
        assertTrue(m.isSetPlugin("spatial"));
        SpatialModelPlugin plugin = (SpatialModelPlugin) m.getPlugin("spatial");
        assertNotNull(plugin);
        List<Compartment> compartments = m.getListOfCompartments();
        Compartment c = compartments.get(0);
        double d = c.getSpatialDimensions();
        assertTrue(3.0 == d);
    }
}
