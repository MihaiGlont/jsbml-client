package net.biomodels;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.*;
import java.lang.Process;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * @author Mihai Glonț <mglont@ebi.ac.uk>
 *         Date: 21/01/15
 */
@RunWith(JUnit4.class)
public class JarExecutionTest {
    private static boolean shouldRun = true;
    private static final String TARGET_FOLDER = "target";
    private static final String JAR_NAME = "jsbml-client-0.1-SNAPSHOT-jar-with-dependencies.jar";
    private static final File f = new File(TARGET_FOLDER, JAR_NAME);

    @BeforeClass
    public static void init() {
        if (!f.exists()) {
            shouldRun = false;
        }
    }

    @Test
    public void testJSbmlFromJar() {
        if (shouldRun) {
            Runtime rt = Runtime.getRuntime();
            try {
                String javaCmd = "/opt/jdk7/bin/java";
                StringBuilder sb = new StringBuilder(javaCmd).append(" -jar ").append(
                        f.getAbsolutePath()).append(" net.biomodels.SpatialPackageTest");
                Process p = rt.exec(sb.toString());
                int exitCode = Integer.MIN_VALUE;
                try {
                    exitCode = p.waitFor();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    fail("your code sucks");
                }
                System.out.println("exitCode = " + exitCode);
            } catch (IOException e) {
                e.printStackTrace();
                fail("your code really sucks");
            }
        }
    }

    @AfterClass
    public static void destroy() {

    }
}
