package net.biomodels;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.sbml.jsbml.ListOf;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ext.fbc.FBCModelPlugin;
import org.sbml.jsbml.ext.fbc.FluxBound;

import static org.junit.Assert.*;

/**
 * @author Mihai Glonț <mglont@ebi.ac.uk>
 *         Date: 15/01/15
 */
@RunWith(JUnit4.class)
public class FbcPackageTest {
    private final App app = new App();
    private Model m;

    @Before
    public void setUp() {
        String fileName = "fbc_example1.xml";
        m = app.unmarshall(fileName);
    }

    @After
    public void tearDown() {
        m = null;
    }

    @Test
    public void testFBC() {
        assertNotNull(m);
        assertTrue(m.isSetPlugin("fbc"));
        FBCModelPlugin plugin = (FBCModelPlugin) m.getPlugin("fbc");
        assertNotNull(plugin);
        ListOf<FluxBound> fluxBoundsList = plugin.getListOfFluxBounds();
        assertEquals(1, fluxBoundsList.size());
        FluxBound fb = fluxBoundsList.get(0);
        assertEquals("bound1", fb.getId());
        assertEquals("J0", fb.getReaction());
        assertEquals("EQUAL", fb.getOperation().name());
        assertTrue(10.0d == fb.getValue());
    }
}
