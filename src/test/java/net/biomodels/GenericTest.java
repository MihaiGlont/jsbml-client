package net.biomodels;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.sbml.jsbml.Model;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

/**
 * Unit test for simple App.
 */
@RunWith(Parameterized.class)
public class GenericTest
{
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"fbc_example1.xml", "Sample FBC model"},
                {"BIOMD0000000412.xml", "Pokhilko2012_CircClock_RepressilatorFeedbackloop"},
                {"BMID000000140325.xml",
                        "Whole Genome Metabolism - Ammonifex degensii (strain DSM 10501 / KC4)"}
        });
    }

    @Parameterized.Parameter
    public String fileName;
    @Parameterized.Parameter(value = 1)
    public String expectedValue;
    private Model m;
    private static final App app = new App();

    @Before
    public void setUp() {
        m = app.unmarshall(fileName);
    }

    @After
    public void tearDown() {
        m = null;
    }

    @Test
    public void testModelNames() {
        assertEquals(expectedValue, app.getElementName(m));
    }
}
