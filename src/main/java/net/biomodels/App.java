package net.biomodels;

import org.sbml.jsbml.Model;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.SBMLReader;

import javax.xml.stream.XMLStreamException;
import java.io.InputStream;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) {
        App app = new App();
        app.run("BIOMD0000000412.xml");
        app.run("fbc_example1.xml");
    }

    private void run(String fileName) {
        final Model m = unmarshall(fileName);
        String modelId = getElementId(m);
        String modelName = getElementName(m);
        System.out.println(modelId);
        System.out.println(modelName);
    }

    Model unmarshall(String fileName) {
        InputStream is = this.getClass().getClassLoader().getResourceAsStream(
                fileName);
        Model model;
        try {
            SBMLDocument document = new SBMLReader().readSBMLFromStream(is);
            model = document.getModel();
            return model;
        } catch (XMLStreamException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getElementId(Model m) {
        return m != null ? m.getId() : null;
    }

    public String getElementName(Model m) {
        return m != null ? m.getName() : null;
    }
}
